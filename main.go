package main

import (
	"errors"
	"fmt"
)

func main() {
	x := "-12 + 10"

	result, err := countFromString(x)

	if err != nil {
		fmt.Println("Error occured: ", err)
	} else {
		fmt.Println(result)
	}
}

func countFromString(x string) (int, error) {
	err := checkString(x)
	length := getLength(x)
	total := 0

	if length == 1 {
		total = int(x[0] - 48)
	} else {
		for i := 0; i < length; i++ {
			char := x[i]

			if i == 0 {
				subLength := 0
				for j := 1; j < length; j++ {
					if x[j-1] == 32 {
						break
					}
					if x[j] == 32 {
						subLength = subLength + j
					}
				}
				// if first character is plus
				if x[i] == 43 {
					for j := subLength - 1; j >= 1; j-- {
						number := getMultiplication(subLength - 1 - j)
						total = total + (int(x[j]-48) * number)
					}
					// if first character is minus
				} else if x[i] == 45 {
					for j := subLength - 1; j >= 1; j-- {
						number := getMultiplication(subLength - 1 - j)
						total = total + (int(x[j]-48) * number)
					}
					total = total * (-1)
				} else {
					for j := subLength - 1; j >= 0; j-- {
						number := getMultiplication(subLength - 1 - j)
						total = total + (int(x[j]-48) * number)
					}
				}
			}

			// if plus
			if i != 0 && char == 43 {
				nextChar := i + 2
				subLength := 0

				for j := nextChar; j <= length; j++ {
					if j == length || x[j] == 32 {
						subLength = subLength + j - nextChar
						break
					}
				}

				subNextChar := subLength + nextChar - 1
				for j := subNextChar; j >= nextChar; j-- {
					number := getMultiplication(subNextChar - j)
					total = total + (int(x[j]-48) * number)
				}
			}

			// if minus
			if i != 0 && char == 45 {
				nextChar := i + 2
				subLength := 0

				for j := nextChar; j <= length; j++ {
					if j == length || x[j] == 32 {
						subLength = subLength + j - nextChar
						break
					}

				}

				subNextChar := subLength + nextChar - 1
				for j := subNextChar; j >= nextChar; j-- {
					number := getMultiplication(subNextChar - j)

					total = total - (int(x[j]-48) * number)
				}
			}
		}

	}

	return total, err
}

// get the result of a multiple of 10
func getMultiplication(number int) int {
	total := 1
	for i := 1; i <= number; i++ {
		total = total * 10
	}
	return total
}

// get input length
func getLength(stringOperation string) int {
	length := 0
	for i := range stringOperation {
		length += 1 + i - i
	}
	return length
}

// check allowed string
func checkString(stringOperation string) error {
	allowedChar := [13]byte{32, 43, 45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57}
	total := 0
	for i := range stringOperation {
		for j := range allowedChar {

			if stringOperation[i] == allowedChar[j] {
				total += 1
			} else {
				total += 0
			}
		}
		if total < i+1 {
			return errors.New("Wrong Input. You can only input numbers, spaces, plus and minus signs.")
		}
	}
	return nil
}
