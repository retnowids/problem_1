**How to run the application:**

1. Clone this repository (make sure you have installed Go on your computer)
2. To run the application, please use the following command

    `go run main.go`

3. You can change the input value by changing the `x` variable in the `main()` function in the `main.go` file

4. To run all the unit test cases, please do the following

    `go run main.go`

    `go test -v`

Hope everything works properly. Thank you.