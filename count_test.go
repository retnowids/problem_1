package main

import (
	"errors"
	"testing"
)

func TestCountingOneDigitCase(t *testing.T) {
	input, expected := "1 + 2 - 1 + 9", 11
	result, err := countFromString(input)

	if result != expected || err != nil {
		t.Errorf("The result of CountingCase(%q) == %d, expected %d", input, result, expected)
	}
}

func TestCountingMoreThanOneDigitCase(t *testing.T) {
	input, expected := "100 - 25 + 30", 105
	result, err := countFromString(input)

	if result != expected || err != nil {
		t.Errorf("The result of CountingCase(%q) == %d, expected %d", input, result, expected)
	}
}

func TestCountingOneNumberOnlyCase(t *testing.T) {
	input, expected := "2", 2
	result, err := countFromString(input)

	if result != expected || err != nil {
		t.Errorf("The result of CountingCase(%q) == %d, expected %d", input, result, expected)
	}
}

func TestEmptyStringCase(t *testing.T) {
	input, expected := "", 0
	result, err := countFromString(input)

	if result != expected || err != nil {
		t.Errorf("The result of CountingCase(%q) == %d, expected %d", input, result, expected)
	}
}

func TestFirstCharacterIsPlusCase(t *testing.T) {
	input, expected := "+12 + 10", 22
	result, err := countFromString(input)

	if result != expected || err != nil {
		t.Errorf("The result of CountingCase(%q) == %d, expected %d", input, result, expected)
	}
}

func TestFirstCharacterIsMinusCase(t *testing.T) {
	input, expected := "-12 + 10", -2
	result, err := countFromString(input)

	if result != expected || err != nil {
		t.Errorf("The result of CountingCase(%q) == %d, expected %d", input, result, expected)
	}
}

func TestErrorCase(t *testing.T) {
	input, expected := "100 - 25 + 30 b", errors.New("Wrong Input. You can only input numbers, spaces, plus and minus signs.")
	_, err := countFromString(input)

	if err == nil {
		t.Errorf("The error for counting case %q should be %q, but found %q", input, expected, err)
	}
}

func TestMultiplicationCase(t *testing.T) {
	input, expected := 3, 1000
	result := getMultiplication(input)

	if result != expected {
		t.Errorf("The result of getMultiplications(%d) == %d, expected %d", input, result, expected)
	}
}

func TestGetLengthCase(t *testing.T) {
	input, expected := "12345", 5
	result := getLength(input)

	if result != expected {
		t.Errorf("The result of getLength(%q) == %d, expected %d", input, result, expected)
	}
}

func TestCheckStringCase(t *testing.T) {
	input, expected := "1234", "nil"
	err := checkString(input)

	if err != nil {
		t.Errorf("The error for counting case %q should be %q, but found %q", input, expected, err)
	}
}
